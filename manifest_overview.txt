MANIFEST CARD GAME

-Competetive card game for 2 (or more?) players.
-Each player has two decks, the NORMAL DECK and the SPECIAL DECK and a CORE CARD
-The NORMAL DECK must include at least 40 cards
-The SPECIAL DECK msut include at least 25 cards

STRUCTURE OF THE GAME

	-before the game starts properly both plyers decide who will go first
	-both players draw 8 cards from their NORMAL DECK
	-both players put their CORE CADR in front of them. the CORE CARD will serve as the nexus of their play, all cards a player plays must either be played adjacent to their respective CORE CARD or adjacent to another card they already played
	-the game is divided into TURNS that alternate between players

